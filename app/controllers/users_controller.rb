# encoding: utf-8

class UsersController < ApplicationController
  before_filter :sign_in_user, only: [:edit, :update]
  before_filter :guest_user, only: [:new, :create]
  before_filter :correct_user, only: [:edit, :update]

  def new
    @user = User.new
    if params[:investor] == 'y'
      @user.investor = true
    else
      @user.investor = false
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      flash['success'] = '恭喜您注册成功！'
      sign_in @user
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
  end

  def index
    @users = User.all
  end

  def update
    if @user.update_attributes(params[:user])
      flash[:success] = '资料已更新'
      sign_in @user
      redirect_to @user
    else
      render 'edit'
    end
  end

  private
    def sign_in_user
      unless signed_in?
        store_location
        flash['info'] = '请您先登录'
        redirect_to signin_path
      end
    end

    def guest_user
      if signed_in?
        flash['warning'] = '您处在登录状态, 请您注销后再执行相关操作'
        redirect_to root_path
      end
    end

    def correct_user
      @user = User.find(params[:id])
      unless current_user?(@user)
        flash['error'] = '您没有权限访问别人的资源'
        redirect_to root_path
      end
    end

end