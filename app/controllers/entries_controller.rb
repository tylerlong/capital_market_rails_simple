# encoding: utf-8

class EntriesController < ApplicationController
  before_filter :sign_in_user, only: [:new, :create]

  def index
    @entries = Entry.all
  end

  def new
    @entry = Entry.new(the_type: '股权')
  end

  def create
    @entry = Entry.new(params[:entry])
    if @entry.save
      flash['success'] = '信息发布成功！'
      redirect_to @entry
    else
      render 'new'
    end
  end

  def show
    @entry = Entry.find(params[:id])
  end

  private

    def sign_in_user
      unless signed_in?
        store_location
        flash['info'] = '请您先登录'
        redirect_to signin_path
      end
    end

end
