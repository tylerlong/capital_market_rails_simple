# encoding: utf-8

class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      sign_in user
      flash[:success] = '您已成功登录'
      redirect_back_or user
    else
      flash.now[:error] = '邮箱或者密码错误'
      render 'new'
    end
  end

  def destroy
    sign_out
    flash[:success] = '您已成功注销'
    redirect_to root_path
  end
end