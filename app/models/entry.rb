# encoding: utf-8

# == Schema Information
#
# Table name: entries
#
#  id             :integer         not null, primary key
#  the_type       :string(255)
#  amount         :string(255)
#  period         :string(255)
#  interest_rates :string(255)
#  description    :text
#  created_at     :datetime        not null
#  updated_at     :datetime        not null
#


class Entry < ActiveRecord::Base
  attr_accessible :amount, :description, :interest_rates, :period, :the_type

  validates :the_type,  presence: true

  validates :amount, presence: true
  validates :period, presence: true

  validates :description, presence: true, length: { minimum: 50 }
end