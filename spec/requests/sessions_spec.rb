# encoding: utf-8

require 'spec_helper'

describe "Sessions" do

  subject { page }

  describe "signin page" do
    before { visit signin_path }
    it { should have_selector('h1',    text: '用户登录') }
    it { should have_selector('title', text: '用户登录') }
  end

  describe "signin" do
    before { visit signin_path }
    describe "with invalid information" do
      before { click_button "登录" }
      it { should have_selector('title', text: '用户登录') }
      it { should have_selector('div.alert.alert-error', text: '邮箱或者密码错误') }
      describe "after visiting another page" do
        before { click_link "富甲天下" }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end
    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",    with: user.email
        fill_in "Password", with: user.password
        click_button "登录"
      end
      it { should have_selector('title', text: user.name) }
      it { should have_link('主页', href: user_path(user)) }
      it { should have_link('修改', href: edit_user_path(user)) }
      it { should have_link('注销', href: signout_path) }
      it { should_not have_link('登录', href: signin_path) }
      describe "followed by signout" do
        before { click_link "注销" }
        it { should have_link('登录') }
      end
    end
  end

  describe "authorization" do
    describe "for non-signed-in users" do
      let(:user) { FactoryGirl.create(:user) }
      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(user)
          sign_in user
        end
        describe "after signing in" do
          it "should render the desired protected page" do
            page.should have_selector('title', text: '修改')
          end
          describe "when signing in again" do
            before do
              click_link "注销"
              sign_in user
            end
            it "should render user profile page" do
              page.should have_selector('title', text: user.name)
            end
          end
        end
      end
      describe "visiting the user edit" do
        before { visit edit_user_path(user) }
        it { should have_selector('title', text: '登录') }
      end
      describe "updating an user profile" do
        before { put user_path(user) }
        specify { response.should redirect_to(signin_path) }
      end
    end
    describe "as wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before { sign_in user }
      describe "visiting Users#edit page" do
        before { visit edit_user_path(wrong_user) }
        it { should_not have_selector('title', text: '修改') }
      end
      describe "submitting a PUT request to the Users#update action" do
        before { put user_path(wrong_user) }
        specify { response.should redirect_to(root_path) }
      end
    end
    describe "as signed-in user" do
      let(:user) { FactoryGirl.create(:user) }
      before { sign_in user }
      describe "try to access the signup_path" do
        before { visit signup_path }
        it { should have_content("欢迎") }
      end
      describe "submitting a POST request to the Users#create action" do
        before { post users_path }
        specify { response.should redirect_to(root_path) }
      end
    end
  end

end