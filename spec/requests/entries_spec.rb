# encoding: utf-8

require 'spec_helper'

describe "Entries" do

  subject { page }

  describe "entries list" do
    before { visit entries_path }
    it { should have_selector('title', text: '资金需求信息一览') }
  end

  describe "new entry" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in user
      visit new_entry_path
    end
    it { should have_selector('title', text: '发布资金需求信息')}
  end
end
