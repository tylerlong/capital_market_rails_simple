# encoding: utf-8

require 'spec_helper'

describe "Static pages" do

  before { visit root_path }
  subject { page }

  describe "home page" do
    it { should have_selector("title", text: "欢迎来到富甲天下") }
  end
end
