# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :entry do
    type ""
    amount "MyString"
    period "MyString"
    interest_rates "MyString"
    description "MyText"
  end
end
