# encoding: utf-8

# == Schema Information
#
# Table name: entries
#
#  id             :integer         not null, primary key
#  the_type       :string(255)
#  amount         :string(255)
#  period         :string(255)
#  interest_rates :string(255)
#  description    :text
#  created_at     :datetime        not null
#  updated_at     :datetime        not null
#


require 'spec_helper'

describe Entry do
  before { @entry = Entry.new(the_type: "股权", amount: "1500万",
                     period: "1年期", interest_rates: "20%", description: "some text "*100) }
  subject { @entry }

  it { should respond_to(:the_type) }
  it { should respond_to(:amount) }
  it { should respond_to(:period) }
  it { should respond_to(:interest_rates) }
  it { should respond_to(:description) }

  it { should be_valid }
end
