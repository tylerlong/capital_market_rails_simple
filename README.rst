==============
Capital Market
==============

A project developed by `Tyler Long`_ when he was attending the Startup Weekend Shenzhen 2012.

.. _`Tyler Long`: http://tylerlong.me

The Chinese name is "富甲天下", there is a demo running `here`_

.. _`here`: http://capital-market.herokuapp.com

People from China should add the following to your hosts file in case the web page loads too slowly:

::

    203.208.46.144 themes.googleusercontent.com