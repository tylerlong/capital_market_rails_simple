class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.string :the_type
      t.string :amount
      t.string :period
      t.string :interest_rates
      t.text :description

      t.timestamps
    end
  end
end
