# encoding: utf-8

namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do


    user = User.create!(name: "蓝海创投",
                 email: "example@example.com",
                 password: "123456",
                 password_confirmation: "123456",
                 investor: true)

    user = User.create!(name: "红杉资本",
                 email: "example2@example.com",
                 password: "123456",
                 password_confirmation: "123456",
                 investor: true)

    user = User.create!(name: "蓝投集团",
                 email: "example3@example.com",
                 password: "123456",
                 password_confirmation: "123456",
                 investor: true)

    user = User.create!(name: "个人投资者",
                 email: "example4@example.com",
                 password: "123456",
                 password_confirmation: "123456",
                 investor: true)

    26.times do |n|
      name  = "#{Faker::Name.name} 投资公司"
      email = "example-#{n+1}@example.com"
      password  = "123456"
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password,
                   investor: true)
    end


    entry1 = Entry.create!(the_type: '股权转让', amount: '1500万', period: '2年期', interest_rates: '', description: Faker::Lorem.sentence(200))
    entry1 = Entry.create!(the_type: '股权融资', amount: '1500万', period: '2年期', interest_rates: '', description: Faker::Lorem.sentence(200))
    entry2 = Entry.create!(the_type: '小额借款', amount: '500万', period: '1年期', interest_rates: '16%', description: Faker::Lorem.sentence(200))
    entry3 = Entry.create!(the_type: '矿产资源转让', amount: '3000万', period: '1年期', interest_rates: '15%', description: Faker::Lorem.sentence(200))
    entry4 = Entry.create!(the_type: '长期借款', amount: '2亿', period: '3年期', interest_rates: '25%', description: Faker::Lorem.sentence(200))

    26.times do |n|
      entry4 = Entry.create!(the_type: '小额借款', amount: '1亿', period: '2年期', interest_rates: '19%', description: Faker::Lorem.sentence(200))
    end


  end
end